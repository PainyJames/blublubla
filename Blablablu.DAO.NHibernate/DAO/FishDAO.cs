﻿using Blablablu.DAO.Core;
using Blablablu.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blablablu.DAO.NHibernate
{
    public class FishDAO : IFishDAO
    {

        public Fish Get(Fish t)
        {
            return new Cod();
        }

        public IList<Fish> GetAll()
        {
            return new List<Fish>() { new Cod(), new Haddock() };
        }

        public Fish Add(Fish t)
        {
            throw new NotImplementedException();
        }

        public Fish Update(Fish t)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Fish t)
        {
            throw new NotImplementedException();
        }
    }
}
