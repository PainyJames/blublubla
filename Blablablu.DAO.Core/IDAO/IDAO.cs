﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blablablu.DAO.Core
{
    public interface IDAO<T>
    {
        T Get(T t);
        IList<T> GetAll();
        T Add(T t);
        T Update(T t);
        bool Delete(T t);
    }
}
