﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blablablu.Service.Core
{
    public interface IService<T>
    {
        T Get(T t);
        IList<T> GetAll();
        T Add(T t);
        T Update(T t);
        bool Delete(T t);
    }
}
