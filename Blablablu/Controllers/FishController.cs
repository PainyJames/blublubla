﻿using Blablablu.Domain;
using Blablablu.Service.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Blablablu.Controllers
{
    public class FishController : ApiController
    {

        public IFishService fishService { get; set; }

        // GET api/<controller>
        public IEnumerable<Fish> Get()
        {
            return fishService.GetAll();
        }

        // GET api/<controller>/5
        public Fish Get(int id)
        {
            return fishService.Get(new Cod());
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}